import * as React from "react";
import { ContentUtils } from "../contentUtils";
import DragAndDrop from "./dragAndDrop";

interface Props {}

export const ImageForm: React.FunctionComponent<Props> = (props: Props) => {
  const [selectedImage, setSelectedImage] = React.useState<string>("");
  const [selectedFile, setSelectedFile] = React.useState<File | null>(null);

  const [imgUrl, setImgUrl] = React.useState<string>("");

  const onFileInputChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.item(0);

    if (file) {
      setSelectedFile(file);

      const b64 = await ContentUtils.getB64FromFile(file);

      setSelectedImage(b64);
      setImgUrl("");
    }
  };

  const onImgUrlChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const url = event.target.value;

    setSelectedImage(url);
    setImgUrl(url);
    setSelectedFile(null);
  };

  const onDrop = async (files: FileList) => {
    const file = files.item(0);

    if (file) {
      setSelectedFile(file);

      const b64 = await ContentUtils.getB64FromFile(file);

      setSelectedImage(b64);
      setImgUrl("");
    }
  };

  const onDrawClick = () => {
    console.log(selectedFile);
  };

  return (
    <DragAndDrop handleDrop={onDrop} className="image-form">
      <div className="image-inputs">
        <input className="image-url" type="text" value={imgUrl} placeholder="Image URL" onChange={onImgUrlChange} />
        <label className="image-upload">
          <input type="file" accept="image/*" onChange={onFileInputChange} />
          Local image
        </label>
      </div>

      <img src={selectedImage} />

      <button className="draw-image-btn" onClick={onDrawClick}>
        Draw image
      </button>
    </DragAndDrop>
  );
};
