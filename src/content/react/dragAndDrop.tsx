import React, { Component } from "react";

interface Props {
  handleDrop?: (files: FileList) => void;
  children: React.ReactNode;
  className?: string;
}

interface State {
  dragging: boolean;
}

class DragAndDrop extends Component<Props, State> {
  private dropRef: React.RefObject<HTMLDivElement>;

  private dragCounter: number = 0;

  /**
   *
   */
  constructor(props: Props) {
    super(props);

    this.state = {
      dragging: false,
    };

    this.dropRef = React.createRef();

    this.handleDrag = this.handleDrag.bind(this);
    this.handleDragIn = this.handleDragIn.bind(this);
    this.handleDragOut = this.handleDragOut.bind(this);
    this.handleDrop = this.handleDrop.bind(this);
  }

  handleDrag = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();
  };

  handleDragIn = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();

    this.dragCounter++;

    if (e.dataTransfer && e.dataTransfer.items && e.dataTransfer.items.length > 0) {
      this.setState({ dragging: true });
    }
  };

  handleDragOut = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();

    this.dragCounter--;

    if (this.dragCounter === 0) {
      this.setState({ dragging: false });
    }
  };

  handleDrop = (e: DragEvent) => {
    e.preventDefault();
    e.stopPropagation();

    this.setState({ dragging: false });

    if (e.dataTransfer && e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      this.props.handleDrop && this.props.handleDrop(e.dataTransfer.files);

      e.dataTransfer.clearData();

      this.dragCounter = 0;
    }
  };

  componentDidMount() {
    let div = this.dropRef.current!;

    div.addEventListener("dragenter", this.handleDragIn);
    div.addEventListener("dragleave", this.handleDragOut);
    div.addEventListener("dragover", this.handleDrag);
    div.addEventListener("drop", this.handleDrop);
  }

  componentWillUnmount() {
    let div = this.dropRef.current!;

    div.removeEventListener("dragenter", this.handleDragIn);
    div.removeEventListener("dragleave", this.handleDragOut);
    div.removeEventListener("dragover", this.handleDrag);
    div.removeEventListener("drop", this.handleDrop);
  }

  render() {
    return (
      <div ref={this.dropRef} className={this.props.className}>
        {this.props.children}
      </div>
    );
  }
}
export default DragAndDrop;
