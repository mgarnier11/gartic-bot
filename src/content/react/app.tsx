import * as React from "react";
import "./app.scss";
import { AppState, Command, Message } from "../../interfaces";
import { ContentUtils } from "../contentUtils";
import { ImageForm } from "./imageForm";
import { CoordinatesForm } from "./coordinatesForm";
import { DrawingStats } from "./drawingStats";

interface Props {}

interface State {
  backgroundAppState: AppState;
}
export class App extends React.Component<Props, State> {
  /**
   *
   */
  constructor(props: Props) {
    super(props);

    this.state = {
      backgroundAppState: AppState.Unknown,
    };

    this.onBackgroundMessage = this.onBackgroundMessage.bind(this);
  }

  componentDidMount(): void {
    chrome.runtime.onMessage.addListener(this.onBackgroundMessage);

    ContentUtils.sendBackgroundMessage(Command.getAppState);
  }

  render() {
    const { backgroundAppState } = this.state;

    return (
      <div
        className="container"
        // hidden={backgroundAppState === AppState.WaitingForDrawingContainer || backgroundAppState === AppState.Unknown}
      >
        {/* {this.renderSwitch()} */}
        <ImageForm />
      </div>
    );
  }

  renderSwitch() {
    const { backgroundAppState } = this.state;

    switch (backgroundAppState) {
      case AppState.WaitingForImage:
        return <ImageForm />;
      case AppState.WaitingForCoordinates:
        return <CoordinatesForm />;
      case AppState.Drawing:
        return <div>Drawing...</div>;
      case AppState.DrawingFinished:
        return <DrawingStats />;
      default:
        return <div>Unknown state</div>;
    }
  }

  private onBackgroundMessage(message: Message) {
    console.log("background message received", message);

    switch (message.command) {
      case Command.appStateChanged:
        this.setState({ backgroundAppState: message.datas as AppState });
        break;
    }
  }
}
