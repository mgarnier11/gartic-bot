import React from "react";
import { createRoot } from "react-dom/client";
import { Command, Message } from "../interfaces";
import { Utils } from "../utils";
import { ContentUtils } from "./contentUtils";
import { App } from "./react/app";

const body = document.querySelector("body");
let contentElement: HTMLElement;

console.log("content script loaded");

const onContentElementChange = () => {
  console.log("content element changed");

  const drawingContainer = document.querySelector(".drawingContainer") as HTMLDivElement;

  if (drawingContainer !== null) {
    ContentUtils.sendBackgroundMessage(Command.drawingContainerFound);
  } else {
    ContentUtils.sendBackgroundMessage(Command.drawingContainerNotFound);
  }
};

window.addEventListener("load", () => {
  const element = document.createElement("div");

  contentElement = document.querySelector("#content") as HTMLElement;
  ContentUtils.observeElement(contentElement, onContentElementChange);

  ContentUtils.sendBackgroundMessage(Command.attachDebugger);

  body?.appendChild(element);

  const root = createRoot(element);

  root.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>
  );
});

window.addEventListener("unload", () => {
  ContentUtils.sendBackgroundMessage(Command.detachDebugger);
});
