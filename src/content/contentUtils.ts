import { Command, Message } from "../interfaces";

export class ContentUtils {
  static sendBackgroundMessage(command: Command, datas?: any) {
    chrome.runtime.sendMessage({ command, datas } as Message);
  }

  static observeElement(element: HTMLElement, cb: () => void) {
    const mutationObserver = new window.MutationObserver(cb);

    mutationObserver.observe(element, { childList: true, subtree: true });
  }

  static getB64FromFile(file: File): Promise<string> {
    return new Promise<string>((res) => {
      if (file) {
        const reader = new FileReader();

        reader.onload = () => {
          res(reader.result as string);
        };

        reader.readAsDataURL(file);
      } else {
        return "";
      }
    });
  }
}
