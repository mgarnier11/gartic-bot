import { AppState, Command, Message } from "../interfaces";
import { BackgroundUtils } from "./backgroundUtils";
import { GameEvents, GameStatus } from "./gameStatus";

const gameStatus = new GameStatus();

let targetTabId: number | undefined = undefined;

const t =
  "https://img.freepik.com/vecteurs-premium/tigre-couche-isole-style-cartoon-illustration-zoologie-educative-image-livre-coloriage_99025-78.jpg";

BackgroundUtils.getBase64FromUrl(t).then((data) => {
  console.log("Base64 : ", data);
});

gameStatus.on(GameEvents.stateChanged, (newState: AppState) => {
  console.log("State changed to", newState);

  BackgroundUtils.sendProcessMessage(targetTabId!, Command.appStateChanged, newState);
});

chrome.runtime.onMessage.addListener(async (message: Message, sender) => {
  console.log("Command received", message);

  if (message.command === Command.attachDebugger) {
    if (targetTabId !== undefined) {
      console.log("Debugger already attached to tab", targetTabId);

      await BackgroundUtils.detachDebugger(targetTabId);
    }

    chrome.tabs.query({ active: true, currentWindow: true }, async (tabs: chrome.tabs.Tab[]) => {
      if (tabs.length > 0) {
        targetTabId = await BackgroundUtils.attachDebugger(tabs[0].id!);
      }
    });
  }

  if (targetTabId !== undefined) {
    switch (message.command) {
      case Command.detachDebugger:
        if (targetTabId !== undefined) {
          await BackgroundUtils.detachDebugger(targetTabId!);

          targetTabId = undefined;
        }
        break;
      case Command.drawingContainerFound:
        gameStatus.state = AppState.WaitingForImage;
        break;
      case Command.drawingContainerNotFound:
        gameStatus.state = AppState.WaitingForDrawingContainer;
        break;
      default:
        console.log("unknow command : ", message);

        break;
    }
  }
});

chrome.debugger.onDetach.addListener((source, reason) => {
  console.log("debugger detached", source, reason);
  targetTabId = undefined;
});
