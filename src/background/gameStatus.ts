import EventEmitter from "events";
import { AppState, IGameStatus } from "../interfaces";

export enum GameEvents {
  stateChanged = "stateChanged",
}

export class GameStatus extends EventEmitter {
  private _state: AppState;

  public get state(): AppState {
    return this._state;
  }

  public set state(state: AppState) {
    if (this._state !== state) {
      this._state = state;

      this.emit(GameEvents.stateChanged, state);
    }
  }

  constructor() {
    super();

    this._state = AppState.WaitingForDrawingContainer;
  }

  public toInterface(): IGameStatus {
    return {
      state: this._state,
    };
  }

  public toJson(): any {
    return this.toInterface();
  }

  public toString(): string {
    return JSON.stringify(this.toJson());
  }
}
