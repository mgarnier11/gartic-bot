import { Command, Message } from "../interfaces";

export class BackgroundUtils {
  static version = "1.3";

  static sendMouseDownEvent = (x: number, y: number, tabId: number) => {
    chrome.debugger.sendCommand({ tabId }, "Input.dispatchMouseEvent", {
      type: "mousePressed",
      x,
      y,
      button: "left",
      // clickCount: 1,
    });
  };

  static sendMouseMoveEvent = (x: number, y: number, tabId: number) => {
    chrome.debugger.sendCommand({ tabId }, "Input.dispatchMouseEvent", {
      type: "mouseMoved",
      x,
      y,
      buttons: 5,
    });
  };

  static sendMouseUpEvent = (x: number, y: number, tabId: number) => {
    chrome.debugger.sendCommand({ tabId }, "Input.dispatchMouseEvent", {
      type: "mouseReleased",
      x,
      y,
      button: "left",
      // clickCount: 1,
    });
  };

  static attachDebugger = (tabId: number) => {
    return new Promise<number>((res) => {
      console.log("Attaching debugger to tab", tabId);

      chrome.debugger.attach({ tabId }, BackgroundUtils.version, () => {
        console.log("Debugger attached to tab", tabId);

        res(tabId);
      });
    });
  };

  static detachDebugger = (tabId: number) => {
    return new Promise<number>((res) => {
      console.log("Detaching debugger from tab", tabId);

      chrome.debugger.detach({ tabId }, () => {
        console.log("Debugger detached from tab", tabId);

        res(tabId);
      });
    });
  };

  static sendProcessMessage(tabId: number, command: Command, datas: any) {
    chrome.tabs.sendMessage(tabId, {
      command,
      datas,
    } as Message);
  }

  static async getBase64FromUrl(url: string) {
    const data = await fetch(url);
    const blob = await data.blob();

    return new Promise<string | ArrayBuffer | null>((resolve) => {
      const reader = new FileReader();

      reader.readAsDataURL(blob);

      reader.onloadend = () => {
        const base64data = reader.result;

        resolve(base64data);
      };
    });
  }
}
