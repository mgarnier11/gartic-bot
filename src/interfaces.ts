export enum Command {
  attachDebugger = "attachDebugger",
  detachDebugger = "detachDebugger",
  drawingContainerFound = "drawingContainerFound",
  drawingContainerNotFound = "drawingContainerNotFound",
  appStateChanged = "appStateChanged",
  getAppState = "getAppState",
}
export interface Message {
  command: Command;
  datas?: any;
}

export enum AppState {
  Unknown = "Unknown",
  WaitingForDrawingContainer = "WaitingForDrawingContainer",
  WaitingForImage = "WaitingForImage",
  WaitingForCoordinates = "WaitingForCoordinates",
  Drawing = "Drawing",
  DrawingFinished = "DrawingFinished",
}

export interface IGameStatus {
  state: AppState;
}
